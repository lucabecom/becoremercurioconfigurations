var Application = angular.module('App', ['angularFileUpload']);


Application.controller("RispostaARichiesta",function($scope,$fileUploader,$http){
    $scope.Allegati = [];

    $scope.RimuoviAllegato = function(id) {
        $scope.Allegati.splice(id,1);
    };

    var uploader = $scope.uploader = $fileUploader.create({
        scope: $scope, // to automatically update the html. Default: $rootScope
        url: '/'+brandname+'/UploadDocumento',
        autoUpload: true,
        headers: {
            'BrandName': brandname
        },
        formData: [{
            key: 'value'
        }],
        filters: [
            function (item) { // first user filter
                return true;
            }
        ]
    });

    uploader.bind('beforeupload', function (event, item) {
        $scope.WaitMessage = "Attendere il caricamento del file";
    });

    uploader.bind('complete', function (event, xhr, item, response) {
        $scope.WaitMessage = null;
        $scope.Allegati.push(response);
    });

    $scope.InviaMessaggioRisposta = function() {

        var messaggio = {
            CodiceAccesso:codiceaccesso,
            Messaggio:$scope.Messaggio,
            Allegati:$scope.Allegati
        };

        $scope.WaitMessage="Invio in corso attendere";

        $http({
            method: 'POST',
            url: '/'+brandname+"/ElaborazioneRichiesta/InvioDaResponder",
            data: messaggio,
            headers: {
                'BrandName': brandname
            }
        }).success(function(result, status, headers) {
            $scope.WaitMessage = null;
            if (result=="OK") {
                window.location = '/'+brandname+'/SmartOpResponder/RispostaCompletata.html';
            } else {
                $scope.Errore = result;
            }
        }).error(function(data) {
        });


    };

});