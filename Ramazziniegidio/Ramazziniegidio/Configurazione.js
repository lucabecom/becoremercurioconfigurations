
var OpzioniMenu = [];

var ModuloCategoriaClienteAttivo = true;


angular.module('EdcTracker').factory('Configurazione',
    function() {

        var o = this;

        o.Get = function(callback) {
            callback({
                "AppName":"Ramazziniegidio",
                "Title":"Ramazziniegidio : Area riservata",
                "SiteUrl":"http://www.omz.it/",
                "LogoImage":"logo.jpg"
            });
        };

        return o;
    });



angular.module('EdcTracker').factory('ConfigurazioneGestioneClienti',function(){
    var o = {
        UploadLogoImmagine:false,
        LegendaValutazioneQuestionario:false,
        GestioneSubCliente:false,
        LabelSubCliente:""
    };
    return o;
});


function NullOrEmpty(o) {
    if (o==null||o==""||o==undefined) return true;
    return false;
}