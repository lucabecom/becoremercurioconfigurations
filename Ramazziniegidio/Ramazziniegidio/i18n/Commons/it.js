var commons = {
    "Buttons" : {
        "Add" : "Aggiungi",
        "Delete" : "Elimina"       
        
    },
    "PlaceHolders" : {
        "Search" : "Cerca"
    },
    "TableOptions" : {
        "DeleteTooltip" : "Elimina i dati",
        "EditTooltip" : "Modifica i dati",
        "Opzioni" : "Opzioni"
    },
    "Allegati": {
        "Download": "Scarica allegato",
        "UploadArea": "Trascina qui il file da allegare"
        
    },
    "Standard": {
        "Nuovo": "Nuovo",
        "Conferma": "Conferma",
        "Annulla": "Annulla"
        
    }
    
};