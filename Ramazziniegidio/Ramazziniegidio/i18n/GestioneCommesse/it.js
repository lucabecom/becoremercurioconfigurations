var GestioneCommesseLabels = {
    "Resources": {
        "Id": "Codice",
        "Stipula": "Stipula",
        "Decorrenza": "Decorrenza",
        "Cliente": "Cliente",
        "Cig": "Cig",
        "Scadenza": "Scadenza",
        "Valore": "Valore",
        "Chiusa": "Chiusa",
        "Titolo": "Titolo",
        "Opzioni": "Opzioni",
        "Referente": "Referente",
        "Provvigione": "Provvigione",
        "ServiziAssociati": "Servizi Associati",
        "Steps": {
            "Titolo": "Steps",
            "Chiuso": "Chiuso",
            "Descrizione": "Descrizione step",
            "Scadenza": "Scadenza step",
            "Effort": "Effort previsto",
            "ImportoDaFatturare": "Importo da fatturare"
        },
        "AlConsumo": {
            "Titolo": "Al consumo",
            "Chiuso": "Chiuso",
            "Descrizione": "Descrizione attività",
            "Effort": "Effort unitario",
            "ImportoDaFatturare": "Importo da fatturare"

        },
        "Allegati": {
            "Titolo" : "Documenti allegati"
        }

    },
    "List" : {
        
        "Title": "Ordini"
               
    },
    "Edit" : {
        
        "Title": "Ordine"
        
    }
};