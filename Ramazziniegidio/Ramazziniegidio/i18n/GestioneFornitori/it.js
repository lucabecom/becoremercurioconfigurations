var     GestioneFornitoriLabels = {
    "Resources": {
        "Id": "Codice",
        "RagioneSociale": "Ragione sociale",
        "Indirizzo": "Indirizzo",
        "Fax": "Fax",
        "Pec": "Pec",
        "Telefono": "Telefono",
        "CategoriaFornitore": "Categoria",
        "FatturatoPerServizio": "Autovalutazione (da 1 a 10)",
        "ComuniOperanti": {
            "Titolo": "Comuni operanti",
            "Comune": "Comune",
            "CategoriaServizio": "Categoria servizio"
        },
        "Documenti": {
            "Titolo": "Documenti e certificazioni",
            "DocumentoTitolo": "Titolo",
            "TipoDocumento": "Tipo documento",
            "DataScadenza": "Data scadenza",
            "AddButton": "Aggiungi documentazione/certificazione"

        },
        "Parametri": "Parametri",
        "OperatoriEAccessi": {
            "Titolo" : "Operatori e accessi",
            "Nominativo": "Nominativo",
            "Qualifica": "Qualifica",
            "Email": "Email",
            "AddButton": "Aggiungi operatore"

        }

    },
    "List" : {
        
        "Title": "Volontari"
               
    },
    "Edit" : {
        
        "Title": "Volontario"
        
    }
};