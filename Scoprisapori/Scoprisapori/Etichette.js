


var Label_Gestione_Speciali = "Soluzioni";
var Label_Ricerca_Lo_Speciale = "Ricerca la soluzione";
var Label_Crea_Speciale = "Crea una nuova soluzione";
var Label_Speciale = "Soluzione";
var Label_NuovoSpeciale = "Nuova soluzione";
var Label_EliminaSpeciale = "Vuoi eliminare la soluzione ?";
var Label_SpecialeEliminato = "Soluzione eliminata";
var Label_ModificaSpeciale = "Modifica soluzione";
var Label_SpecialeSalvato = "Soluzione salvata correttamente";

var Label_Gestione_Prodotti = "Servizi";
var Label_Ricerca_Il_Prodotto = "Ricerca il servizio";
var Label_Crea_Prodotto = "Crea una servizio";
var Label_Prodotto = "Servizio";
var Label_NuovoProdotto = "Nuovo servizio";
var Label_EliminaProdotto = "Vuoi eliminare il servizio ?";
var Label_ProdottoEliminato = "Servizio eliminato";
var Label_ModificaProdotto = "Modifica servizio";
var Label_ProdottoSalvato = "Servizio salvato correttamente";

var Label_Caratteristiche_Prodotto = "Caratteristiche servizi";


var Label_Prodotto_Allegato = "Scheda tecnica";
var Label_Prodotto_Allegato_Riservato1 = "Allegato riservato 2D";
var Label_Prodotto_Allegato_Riservato2 = "Allegato riservato 3D";