
var OpzioniMenu = [];


var DurataServizi = ["Annuale", "Annuale (Continuativa)", "Spot"];


var Slider_Config = {
    Video: true,
    Notizie: false,
    Slider: true,
    Prodotti: false
}




var Configurazione_Collezioni = {
    Label_Allegato: "Catalogo",
    LabelCollezioni: "Aree servizi",
    LabelCollezione: "Area",
    CodiceAutomatico: false,
    Titolo: true,
    DescrizioneBreve: true,
    DescrizioneCompleta: true,
    Link: false,
    Immagine: true,
    Allegato: false,
    InHome: false,

    Vantaggi: true,
    ProfiloProfessionale: true,
    QualiStrumenti: true,

    UtentiAssociati: true,
    Inserimento: false,
    Elimina: false

};



//Per loro sono configurazione SERVIZI
var Configurazione_Prodotti = {
    Prezzo: false,
    Offerta: false,
    Prenotazione: false,
    Prenotabile: false,
    Acquistabile: false,
    Categorie: false,
    Caratteristiche: false,
    Lingue: true,
    Titolo: true,
    DescrizioneBreve: true,
    DescrizioneCompleta: true,
    Link: false,
    Allegato: false,
    Immagini: true,
    Varianti: false,
    AllegatoRiservato1: false,
    AllegatoRiservato2: false,
    Collezioni: true,
    OpzioneDiAcquisto: false,
    OrdineVisibilita: true,


    AChi:true,
    Lavorazione:true,
    ProfiloProfessionale:true,
    Perche:true,
    Durata:true
};




var Configurazione_Speciali = {
    Categorie: false,
    Link: false,
    Immagine2: true,
    Immagine3: true
};

var Configurazioni_Clienti = {
    InformazioniEsteseFdb:  true
}


var ModuloCategoriaClienteAttivo = true;


angular.module('EdcTracker').factory('Configurazione',
    function() {

        var o = this;

        o.Get = function(callback) {
            callback({
                "AppName":"Scoprisapori	",
                "Title":"Scoprisapori: Area riservata",
                "SiteUrl":"http://www.scoprisapori.it/",
                "LogoImage":"logo.jpg"
            });
        };

        return o;
    });



angular.module('EdcTracker').factory('ConfigurazioneGestioneClienti',function(){
    var o = {
        UploadLogoImmagine:false,
        LegendaValutazioneQuestionario:false,
        GestioneSubCliente:false,
        LabelSubCliente:""
    };
    return o;
});


function NullOrEmpty(o) {
    if (o==null||o==""||o==undefined) return true;
    return false;
}